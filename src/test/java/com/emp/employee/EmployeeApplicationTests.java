package com.emp.employee;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class EmployeeApplicationTests {

	
                        
                        public static void main(String... a){
                            String password = "password";
                            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                            String hashedPassword = passwordEncoder.encode(password);

                            System.out.println(hashedPassword);
                        }

}
