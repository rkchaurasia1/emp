<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style type="text/css">
            body{
                background-image: url('<c:url value="/assets/img/bg/giftly.png"/>');
                font-family: Arial, Helvetica, sans-serif;
                font-size:12px;
            }
            #login-box{
                margin:80px auto 0;
                background-color:#FFF;
                padding:20px;
                box-shadow: 1px 2px 5px rgba(0, 0, 0, 0.15);
            }
            @media(min-width: 600px){
                #login-box{
                    width:36%;
                }
            }

            table{
                font-size:14px;
            }
            table td{
                padding:6px 0;
            }
            table input[type="text"], table input[type="password"]{
                width:100%;
                height:25px;
                font-size:16px;
                border: 1px solid #CCCCCC;
                border-radius: 3px;
                padding:2px;
            }
            table input[type="text"]:focus, table input[type="password"]:focus{
                background: none repeat scroll 0 0 #FAFAFA;
                border-color: #B3B3B3;
                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1) inset;
                transition: all 0.15s linear 0s;
            }
            table input[type="submit"]{
                background: none repeat scroll 0 0 #2BA6CB;
                border: 1px solid #1E728C;
                box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
                color: #FFFFFF;
                cursor: pointer;
                display: inline-block;
                font-family: inherit;
                font-size: 14px;
                font-weight: bold;
                line-height: 1;
                margin: 0 2px;
                position: relative;
                text-align: center;
                text-decoration: none;
                transition: background-color 0.15s ease-in-out 0s;
                width: auto;
                padding: 8px 20px 9px;
                border-radius:3px;
                border: 0 none !important;
                box-shadow: none !important;
            }
            table input[type="submit"]:focus, table input[type="submit"]:hover{
                background-color: #2284A1;
            }
            .error{
                background-color: #FAF2CC;
                color:#8A6D3B;
                text-align: center;
                padding:10px 0;
            }
            .msg{
                background-color:#D0E9C6;
                color:#3C763D;
                text-align: center;
                padding:10px 0;
            }
        </style>
    </head>
    <body onload='document.loginForm.username.focus();'>
        <div id="login-box">
            <div style='text-align:right;'>
                <img src='https://www.winni.in/assets/img/newlogo/logo-test-36-CL-1.png' alt="Winni.in">
            </div>

            <c:if test="${not empty error}">
                <div class="error">${error}</div>
            </c:if>
            <c:if test="${not empty message}">
                <div class="msg">${message}</div>
            </c:if>
            
            <form name='loginForm' action="<c:url var="loginUrl" value="/login" />" method='POST'>
                
                <table width="100%" style="margin-top:10px;">
                    <tr>
                        <td>Username:</td>
                        <td><input type='text' name='username' value=''></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type='password' name='password' /></td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <div style="text-align:right;">
                                <input name="submit" type="submit" value="Sign In" />
                            </div>
                        </td>
                    </tr>
                </table>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

            </form>
            <p style='margin-top:20px; font-size:12px; color:rgb(150,150,150); text-align: justify;'>This site is intended for use by Authorized Users only. Any attempt to deny access to, hack into and/or deface this site will result in criminal prosecution under local, state, federal and international law. If you have reached this website in error, please remove yourself by typing the correct URL name of the website intended. We reserve the right to monitor access to/from this website in accordance with the company's policies.</p>
        </div>
    </body>
</html>
