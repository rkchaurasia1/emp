/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emp.employee.service;

import com.emp.employee.model.UserRoleMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author User-4
 */
@Repository
public interface UserRoleMappingRepository extends JpaRepository<UserRoleMapping, Integer>{
    
}
