/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.emp.employee.controller;

import java.security.Principal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author User-4
 */
@Controller

public class EmployeeController {

    @RequestMapping(value = "/")
    public String login(Model model) {
        
        return "login";

    }
    @RequestMapping(value = "/home")
    public String homePage(Model model, Principal principal) {
        System.out.println(principal.getName());
        return "home-page";

    }
}
